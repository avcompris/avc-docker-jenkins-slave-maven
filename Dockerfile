# File: avc-docker-jenkins-slave-maven/Dockerfile
#
# Use to build the image: avcompris/jenkins-slave-maven

FROM avcompris/jenkins-slave-maven:base
MAINTAINER david.andriana@avantage-compris.com

#-------------------------------------------------------------------------------
#   3. USERS
#-------------------------------------------------------------------------------

USER jenkins
WORKDIR /home/jenkins

#-------------------------------------------------------------------------------
#   4. PRE-FETCHED MAVEN DEPENDENCIES
#-------------------------------------------------------------------------------

# 4.1. Maven plugins

RUN ./get_maven_dependency.sh org.apache.maven.plugins:maven-deploy-plugin:2.7
RUN ./get_maven_dependency.sh org.apache.maven.plugins:maven-site-plugin:3.7.1

# 4.2. Maven libraries

RUN ./get_maven_dependency.sh org.apache.maven.wagon:wagon-ssh:3.3.4

# 4.3. Very commons

RUN ./get_maven_dependency.sh com.google.code.findbugs:jsr305:3.0.0
RUN ./get_maven_dependency.sh com.google.guava:guava:23.0
RUN ./get_maven_dependency.sh commons-codec:commons-codec:1.11
RUN ./get_maven_dependency.sh commons-io:commons-io:2.4
RUN ./get_maven_dependency.sh joda-time:joda-time:2.9.9
RUN ./get_maven_dependency.sh org.apache.commons:commons-lang3:3.4
RUN ./get_maven_dependency.sh org.apache.commons:commons-collections4:4.0
RUN ./get_maven_dependency.sh org.hamcrest:hamcrest:2.2
RUN ./get_maven_dependency.sh org.junit.jupiter:junit-jupiter:5.6.1
RUN ./get_maven_dependency.sh org.junit.jupiter:junit-jupiter-engine:5.3.2
RUN ./get_maven_dependency.sh org.junit.jupiter:junit-jupiter-params:5.3.2
RUN ./get_maven_dependency.sh org.mockito:mockito-core:2.27.0

# 4.4. AvCompris

RUN ./get_maven_dependency.sh net.avcompris.commons:avc-binding-dom:0.1.14
RUN ./get_maven_dependency.sh net.avcompris.commons:avc-domdumper:0.5.3

# 4.5. Others

RUN ./get_maven_dependency.sh com.beust:jcommander:1.72
RUN ./get_maven_dependency.sh com.fasterxml.jackson.core:jackson-databind:2.8.9
RUN ./get_maven_dependency.sh com.googlecode.json-simple:json-simple:1.1.1
RUN ./get_maven_dependency.sh com.zaxxer:HikariCP:3.2.0
RUN ./get_maven_dependency.sh io.rest-assured:rest-assured:3.3.0
RUN ./get_maven_dependency.sh io.springfox:springfox-swagger2:2.6.1
# RUN ./get_maven_dependency.sh net.sourceforge.htmlunit:htmlunit:2.27
# RUN ./get_maven_dependency.sh org.apache.commons:commons-exec:1.3
RUN ./get_maven_dependency.sh org.postgresql:postgresql:9.4.1212
RUN ./get_maven_dependency.sh org.jboss.logging:jboss-logging:3.3.0.Final
RUN ./get_maven_dependency.sh org.seleniumhq.selenium:selenium-server:3.6.0
RUN ./get_maven_dependency.sh org.springframework:spring-web:4.3.10.RELEASE
RUN ./get_maven_dependency.sh org.springframework.boot:spring-boot:1.5.6.RELEASE
RUN ./get_maven_dependency.sh org.springframework.boot:spring-boot-maven-plugin:1.5.6.RELEASE
RUN ./get_maven_dependency.sh org.springframework.boot:spring-boot-starter-tomcat:1.5.6.RELEASE
RUN ./get_maven_dependency.sh org.springframework.boot:spring-boot-starter-web:1.5.6.RELEASE
RUN ./get_maven_dependency.sh org.yaml:snakeyaml:1.24

#-------------------------------------------------------------------------------
# 8. BUILDINFO (RELY ON JENKINS: POPULATED VIA "buildinfo.sh")
#-------------------------------------------------------------------------------

COPY buildinfo /

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

USER root
WORKDIR /



