# avc-docker-jenkins-slave-maven

Docker image: avcompris/jenkins-slave-maven

Usage:

	$ docker run \
		-e JENKINS_PUB_KEY="ssh-rsa AAAAB3Nz...NLsug/a7" \
		avcompris/jenkins-slave-maven

Exposed port is 22.
